#!/bin/bash

pushd firmware
    gzip -r data/www 
    cp data/config/example.wifi.json data/config/wifi.json
    cp data/config/example.mqtt.json data/config/mqtt.json
    if [ "$#" -eq 1 ]; then 
        pio run -e $1 -t buildfs
    else
        pio run -t buildfs
    fi
popd