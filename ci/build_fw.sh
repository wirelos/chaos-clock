#!/bin/bash

pushd firmware
    if [ "$#" -eq 1 ]; then 
        pio run -e $1
    else
        pio run
    fi
popd