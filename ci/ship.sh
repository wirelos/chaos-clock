#!/bin/bash

BINARY_PATH=bin
RELEASE_NAME=release

if [ "$#" -eq 2 ]; then
    RELEASE_NAME=$1
    BINARY_PATH=$2/$RELEASE_NAME
fi

mkdir -p $BINARY_PATH
mv firmware/.pio/build/$RELEASE_NAME/*.bin $BINARY_PATH
