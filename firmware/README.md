# Chaos-Clock

# OTA
```
curl -v -F file=@.pioenvs/basic/firmware.bin http://chaos-clock/update
```

## TODO
- in case of missing network access, do not try to connect to MQTT (OPEN) or NTP server (DONE), as this blocks HTTP requests and results in wdt reset
- incoming matrix updates need to be queued instead, perhaps create one-shot tasks
- refactor all the stuff
    - create separate task classes for IO?
    - separate basic matrix from meeting timer stuff
- fix build warnings
- update ArduinoJson to latest version
- move network into stack
- generalize stack in order to pass implementation (e.g. WebStack) into system
- add heartbeat publisher!