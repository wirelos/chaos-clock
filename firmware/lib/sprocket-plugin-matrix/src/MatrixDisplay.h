#ifndef __MATRIX_DISPLAY__
#define __MATRIX_DISPLAY__

#include <JsonStruct.h>
#include <utils/print.h>

struct MatrixDisplay {
  const char* line1;
  int line1Color;
  int line1Mode;
  const char* line2;
  int line2Color;
  int line2Mode;
};

struct MatrixDisplayJson : public MatrixDisplay, public JsonStruct
{
  String textLine1 = "";
  String textLine2 = "";

  MatrixDisplayJson* fromConfig(MatrixDisplay d) {
    textLine1 = String(d.line1);
    line1 = d.line1;
    line1Color = d.line1Color;
    line1Mode = d.line1Mode;
    line2 = d.line2;
    textLine2 = String(d.line2);
    line2Color = d.line2Color;
    line2Mode = d.line2Mode;
    return this;
  }

     void mapJsonObject(JsonObject& root) {
        root["line1"] = textLine1.c_str();
        root["line1Color"] = line1Color;
        root["line1Mode"] = line1Mode;
        root["line2"] = textLine2.c_str();
        root["line2Color"] = line2Color;
        root["line2Mode"] = line2Mode;
        
    }
    // Map a json object to this struct.
    void fromJsonObject(JsonObject& json){
        if(!verifyJsonObject(json)){
            PRINT_MSG(Serial, "fromJsonObject", "cannot parse JSON");
            valid = 0;
            return;
        }
        textLine1 = getStrAttrFromJson(json, "line1", textLine1);
        line1 = getAttr(json, "line1", line1);
        line1Color = getIntAttrFromJson(json, "line1Color", line1Color);
        line1Mode = getIntAttrFromJson(json, "line1Mode", line1Mode);
        textLine2 = getStrAttrFromJson(json, "line2", textLine2);
        line2 = getAttr(json, "line2", line2);
        line2Color = getIntAttrFromJson(json, "line2Color", line2Color);
        line2Mode = getIntAttrFromJson(json, "line2Mode", line2Mode);
    }
};

#endif