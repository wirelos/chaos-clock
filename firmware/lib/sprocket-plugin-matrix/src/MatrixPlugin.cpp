#include "MatrixPlugin.h"

MatrixPlugin::MatrixPlugin(MatrixConfig cfg)
{
    config = cfg;
    setup();
    textStepTimer->setTime(75);
}

void MatrixPlugin::setup() 
{
    matrix = new Adafruit_NeoMatrix(config.width, config.height, config.pin, config.matrixType, config.ledType);
    matrix->begin();
    matrix->setBrightness(config.brightness);
    matrix->fillScreen(LED_BLUE_MEDIUM);
    // text
    matrix->setFont(&TomThumb);
    
    matrix->setCursor(0,0);
    matrix->setRotation(0);
    matrix->setTextWrap(false);
    matrix->setTextSize(1);
    matrix->setTextColor(LED_GREEN_MEDIUM);
    matrix->fillScreen(LED_BLACK);
    matrix->show();
}

void MatrixPlugin::setText(int x, int y, int size, int wrap, String text, int color) {
    matrix->setTextColor(color);
    matrix->setTextWrap(wrap);
    matrix->setTextSize(size);
    matrix->setCursor(x,y);
    matrix->print(text);
}

int MatrixPlugin::updateScrollTextPosition(int startPos,int endPos,  int posX, String text) {
    int len = ((int)text.length()) + 1;
    if(!(posX-- >= ~(endPos))){
        introRepetitions++; // should be solved as a task
        return startPos;
    }
    return posX;
}

// FIXME y-positioning of scrolltext
void MatrixPlugin::scrollText(int textPosY, int fontSize, int color, String scrollText) {
    //unsigned long timeStamp = millis();
    int16_t x, y;
    uint16_t w, h;
    matrix->getTextBounds((char*)scrollText.c_str(), 0, 0, &x, &y, &w, &h);
    matrix->fillRect(0, textPosY - 5, config.width, textPosY, LED_BLACK);
    scrollTextPosX = updateScrollTextPosition(scrollTextStartPos, w + (2 * config.width), scrollTextPosX, scrollText);
    // tom thumb
    setText(scrollTextPosX, textPosY, fontSize, 0, scrollText, color);
    //setText(scrollTextPosX, textPosY, fontSize, 0, scrollText, LED_WHITE_MEDIUM);
}
void MatrixPlugin::draw(int w, int h, uint16_t* frame, int length){
    for(int index = 0; index < length; index++){
        int x = index % w;
        int y = index / w;
        uint16_t color = frame[index];
        matrix->drawPixel(x, y, color);
    }
}

void MatrixPlugin::setScrollTextSubscriber(String msg){
    text = msg;
    //scrollTextPosX = 0; // reset to start position
}

void MatrixPlugin::line1(String msg){
    if(mode != 1) return;
    PRINT_MSG(Serial, "MATRIX", msg.c_str());
    // + 2 offset from top
    matrix->fillRect(0, 0, config.width, 5 + 1, LED_BLACK);
//    setText(0, 5 + 1, 1, 0, msg, LED_GREEN_HIGH); // start left
    int len = ((int)msg.length()) * 3;
    setText((config.width - len) / 2, 5 + 1, 1, 0, msg, LED_GREEN_HIGH); // start centered
}

void MatrixPlugin::line2(String msg){
    if(mode != 1) return;
    PRINT_MSG(Serial, "MATRIX", msg.c_str());
    // + 2 offset from top
    matrix->fillRect(0, 5 + 1, config.width, 15, LED_BLACK);
    setText(0, 14, 1, 0, msg, LED_CYAN_HIGH);
}

void MatrixPlugin::showInfo() {    
    MatrixStaticImage img = staticImages["smiley"];
    matrix->drawRGBBitmap(img.x, img.y, img.image, img.w, img.h);
    scrollText(14, 1, LED_WHITE_MEDIUM, WiFi.localIP().toString());
    // run again?
    int introRunning = introRepetitions <= maxIntroRepetitions;
    if(!introRunning){
        mode = 1;
        introRepetitions = 0;
        matrix->clear();
    }  
}

void MatrixPlugin::showFixed(MatrixDisplayJson d) {
    if(mode != 1) return;
    matrix->clear();
    // line 1
    if(d.textLine1.length() > 0){
        matrix->fillRect(0, 0, config.width, 5 + 1, LED_BLACK);
        setText(0, 5 + 1, 1, 0, d.textLine1, d.line1Color);
    }

    // line 2
    if(d.textLine2.length() > 0){
        matrix->fillRect(0, 5 + 2, config.width, 14, LED_BLACK);
        setText(0, 14, 1, 0, d.textLine2, d.line2Color);
    }
    matrix->show();
}

void MatrixPlugin::display(String msg){
    PRINT_MSG(Serial, "MATRIX", msg.c_str());
    MatrixDisplayJson d;
    d.fromJsonString(msg);
    showFixed(d);
}

MatrixPlugin* MatrixPlugin::addStaticImage(MatrixStaticImage img)
{
    staticImages[img.name] = img;
    return this;
}

void MatrixPlugin::drawStaticImage(String msg){
    mode = 2;
    PRINT_MSG(Serial, "MATRIX", "drawStaticImage");
    MatrixStaticImage img = staticImages[msg.c_str()];
    matrix->clear();
    matrix->drawRGBBitmap(img.x, img.y, img.image, img.w, img.h);
}

void MatrixPlugin::changeMode(String m)
{
    //PRINT_MSG(Serial, "MATRIX", String("MODE=" + m).c_str());
    //mode = (MATIRX_MODE)atoi(m.c_str());
    mode = atoi(m.c_str());
    PRINT_MSG(Serial, "MATRIX", String("MODE=" + mode).c_str());
    matrix->clear();
}

void MatrixPlugin::activate(Scheduler *scheduler)
{

    line1("  =)");
    mode = 0;
    
    renderTask.set(TASK_MILLISECOND * 50, TASK_FOREVER, [this](){
        
        // TODO use custom tasks
        if(mode == 0){
            showInfo();
        } else if(mode == 1) {
            // TODO change this shit so that we don't keep a "text" var
            scrollText(13, 1, LED_CYAN_HIGH, text);
        } else if(mode == 2) {
            
        }
        /* else if(length > 0){
            draw(config.width, config.height, frameBuffer, length);
        } */
        matrix->show();
    });

    subscribe("matrix/scrollText", bind(&MatrixPlugin::setScrollTextSubscriber, this, _1));
    subscribe("matrix/line1", bind(&MatrixPlugin::line1, this, _1));
    subscribe("matrix/line2", bind(&MatrixPlugin::line2, this, _1));
    subscribe("matrix/display", bind(&MatrixPlugin::display, this, _1));
    subscribe("matrix/drawStaticImage", bind(&MatrixPlugin::drawStaticImage, this, _1));
    subscribe("matrix/changeMode", bind(&MatrixPlugin::changeMode, this, _1));

    scheduler->addTask(renderTask);
    renderTask.enable();

    PRINT_MSG(Serial, "MATRIX", "plugin activated");
}
