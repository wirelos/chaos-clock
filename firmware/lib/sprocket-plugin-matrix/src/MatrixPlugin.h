#ifndef __MATRIX_PLUGIN__
#define __MATRIX_PLUGIN__

#define _TASK_SLEEP_ON_IDLE_RUN
#define _TASK_STD_FUNCTION

#include <Plugin.h>
#include <ESP8266WiFi.h>
#include <Adafruit_GFX.h>
#include <Adafruit_NeoMatrix.h>
#include <Adafruit_NeoPixel.h>
#include "colors.h"
#include <Fonts/TomThumb.h>
#include <Fonts/Org_01.h>
#include <ArduinoJson.h>
#include "JsonStruct.h"
#include "utils/print.h"
#include "MatrixDisplay.h"
#include <vector>

using namespace std;
using namespace std::placeholders;

class DeltaTimer {
private:
  unsigned long ms = 0;
  unsigned long time;
public:
  void setTime(unsigned long updateTime) {
    time = updateTime;
  }

  bool update(unsigned long delta) {
    ms += delta;
    if (ms > time) {
      ms = ms % time;
      return true;
    }
    return false;
  }
};

enum MATIRX_MODE { INTRO = 0, STATIC = 1, DYNAMIC = 2  };

struct MatrixStaticImage
{
  const char* name;
  int x;
  int y;
  int w;
  int h;
  const uint16_t * image;
};

struct MatrixConfig
{
    int height;
    int width;
    int pin;
    int matrixType;
    int ledType;
    int brightness;
};

class MatrixPlugin : public Plugin
{
  public:
    MatrixConfig config;
    Adafruit_NeoMatrix* matrix;
    unsigned long lastTimeStamp = millis();
    uint16_t* frameBuffer;
    int length = 0;
    DeltaTimer *textStepTimer = new DeltaTimer();
    int introRepetitions = 0;
    int maxIntroRepetitions = 1;
    int scrollTextStartPos = 16;
    int scrollTextPosX = 0;
    String text = "";
    Task renderTask;
    int mode = 0;
    std::map<std::string, MatrixStaticImage> staticImages;

    MatrixPlugin(MatrixConfig cfg);
    void setup();
    void showInfo();
    void activate(Scheduler *scheduler);
    void setText(int x, int y, int size, int wrap, String text, int color);
    int updateScrollTextPosition(int startPos,int endPos,  int posX, String text);
    void scrollText(int textPosY, int fontSize, int color, String scrollText);
    void draw(int w, int h, uint16_t* frame, int length);
    void drawStaticImage(String msg);
    MatrixPlugin* addStaticImage(MatrixStaticImage img);
    void changeMode(String mode);

    void showFixed(MatrixDisplayJson d);

    void setScrollTextSubscriber(String msg);
    void line1(String msg);
    void line2(String msg);
    void display(String msg);
};

#endif