#include "MP3PlayerPlugin.h"

MP3PlayerPlugin::MP3PlayerPlugin(int rxPin, int txPin)
{
    softwareSerial = new SoftwareSerial(rxPin, txPin);
}

void MP3PlayerPlugin::activate(Scheduler* s)
{
    softwareSerial->begin(9600);
    if (!player.begin(*softwareSerial)) { 
        PRINT_MSG(Serial, "MP3", "Unable to begin:");
        PRINT_MSG(Serial, "MP3", "1.Please recheck the connection!");
        PRINT_MSG(Serial, "MP3", "2.Please insert the SD card!");
        return;
    }
    PRINT_MSG(Serial, "MP3", "Device available");
    
    player.volume(10);  //Set volume value. From 0 to 30

    subscribe("music/play", [this](String msg){
        player.play(2);
    });
    subscribe("music/pause", [this](String msg){
        player.pause();
    });
    subscribe("music/volume", [this](String msg){
        player.volume(atoi(msg.c_str()));
    });
}