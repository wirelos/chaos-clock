#ifndef __MP3PLAYER_PLUGIN__
#define __MP3PLAYER_PLUGIN__

#define _TASK_SLEEP_ON_IDLE_RUN
#define _TASK_STD_FUNCTION

#include "config.h"
#include "Sprocket.h"
#include "Plugin.h"
#include "SoftwareSerial.h"
#include "DFRobotDFPlayerMini.h"
#include "utils/print.h"

using namespace std;
using namespace std::placeholders;

class MP3PlayerPlugin : public Plugin {
    private:
        SoftwareSerial *softwareSerial;
        DFRobotDFPlayerMini player;
    public: 
        MP3PlayerPlugin(int rxPin, int txPin);
        void activate(Scheduler*);
};

#endif