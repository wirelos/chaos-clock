#include <TimePlugin.h>

TimePlugin::TimePlugin()
{
    ntpServerName = "ch.pool.ntp.org";
    syncInterval = 60000;
    timeZone = 1;
    offset = 1;
}

TimePlugin::TimePlugin(String server, int tz, int off, int interval)
{
    ntpServerName = server;
    syncInterval = interval;
    timeZone = tz;
    offset = off;
}

time_t TimePlugin::getNtpTime()
{
    IPAddress ntpServerIP; // NTP server's ip address
    while (udp.parsePacket() > 0) ; // discard any previously received packets
    PRINT_MSG(Serial, "TIME", "Transmit NTP Request");
    // get a random server from the pool
    WiFi.hostByName(ntpServerName.c_str(), ntpServerIP);
    PRINT_MSG(Serial, "TIME", (ntpServerName + String(": ") + ntpServerIP.toString()).c_str());
    sendNTPpacket(ntpServerIP);
    uint32_t beginWait = millis();
    while (millis() - beginWait < 1500) {
        int size = udp.parsePacket();
        if (size >= NTP_PACKET_SIZE) {
            PRINT_MSG(Serial, "TIME", "Receive NTP Response");
            udp.read(packetBuffer, NTP_PACKET_SIZE);  // read packet into the buffer
            unsigned long secsSince1900;
            // convert four bytes starting at location 40 to a long integer
            secsSince1900 =  (unsigned long)packetBuffer[40] << 24;
            secsSince1900 |= (unsigned long)packetBuffer[41] << 16;
            secsSince1900 |= (unsigned long)packetBuffer[42] << 8;
            secsSince1900 |= (unsigned long)packetBuffer[43];
            return secsSince1900 - 2208988800UL + timeZone * SECS_PER_HOUR;
        }
    }
    PRINT_MSG(Serial, "TIME", "No NTP Response :-(");
    return 0; // return 0 if unable to get the time
}

void TimePlugin::activate(Scheduler* scheduler)
{
    if(!WiFi.isConnected()) {
        PRINT_MSG(Serial, "TIME", "No network connection. Abort.");
        return;
    }

    udp.begin(udpPort);
    PRINT_MSG(Serial, "TIME", "Starting UDP");
    PRINT_MSG(Serial, "TIME", ("Local UDP port: " + udp.localPort()));
    PRINT_MSG(Serial, "TIME", String(udp.localPort()).c_str());
    PRINT_MSG(Serial, "TIME", "waiting for sync");

    subscribe("ntp/toggleUpdate", bind(&TimePlugin::toggleUpdate, this, _1));

    timeSyncTask.set(TASK_MILLISECOND * syncInterval, TASK_FOREVER, bind(&TimePlugin::syncTime, this));
    scheduler->addTask(timeSyncTask);
    timeSyncTask.enable();

    timeUpdateTask.set(TASK_SECOND, TASK_FOREVER, bind(&TimePlugin::updateTime, this));
    scheduler->addTask(timeUpdateTask);
    timeUpdateTask.enable();
}

String TimePlugin::leadingZero(int n)
{
    return n < 10 ? "0" + String(n) : String(n);
}

void TimePlugin::toggleUpdate(String msg)
{
    int enableTask = strcmp(msg.c_str(),"1") == 0;
    if(enableTask){
        PRINT_MSG(Serial, "TIME", "enable");
        timeUpdateTask.enable();
    } else {
        PRINT_MSG(Serial, "TIME", "disable");
        timeUpdateTask.disable();
    }
}

void TimePlugin::updateTime()
{
    if (timeStatus() != timeNotSet) {
        String tShort = leadingZero(hour() + offset) + ":" + leadingZero(minute());
        String tf = tShort + ":" + leadingZero(second());
        String df = leadingZero(day()) + "." + leadingZero(month()) + "." + leadingZero(year());
        String full = df + " / " + tf;
        publish("ntp/timeShort", tShort);
        publish("ntp/time", tf);
        publish("ntp/date", df);
        publish("ntp/full", full);
        PRINT_MSG(Serial, "TIME", String(tf + " / " + df).c_str());
    }
}

void TimePlugin::syncTime()
{
    int ts = getNtpTime();

    if(ts > 0) {
        setTime(ts);
        lastUpdate = millis();
    } else if(millis() - lastUpdate > NTP_RETRY_THRESHOLD) {
        // Try to sync again disregarding normal sync interval,
        // as NTP server could be down
        syncTime();
        lastUpdate = millis();
    }
}

void TimePlugin::sendNTPpacket(IPAddress &address)
{
  // set all bytes in the buffer to 0
  memset(packetBuffer, 0, NTP_PACKET_SIZE);
  // Initialize values needed to form NTP request
  // (see URL above for details on the packets)
  packetBuffer[0] = 0b11100011;     // LI, Version, Mode
  packetBuffer[1] = 0;              // Stratum, or type of clock
  packetBuffer[2] = 6;              // Polling Interval
  packetBuffer[3] = 0xEC;           // Peer Clock Precision
  // 8 bytes of zero for Root Delay & Root Dispersion
  packetBuffer[12] = 49;
  packetBuffer[13] = 0x4E;
  packetBuffer[14] = 49;
  packetBuffer[15] = 52;
  // all NTP fields have been given values, now
  // you can send a packet requesting a timestamp:
  udp.beginPacket(address, 123); //NTP requests are to port 123
  udp.write(packetBuffer, NTP_PACKET_SIZE);
  udp.endPacket();
}