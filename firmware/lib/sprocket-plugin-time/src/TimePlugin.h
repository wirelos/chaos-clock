// Sprocket plugin for NTP time, adapted from Arduino Time lib example.
// Original code: https://github.com/PaulStoffregen/Time/blob/master/examples/TimeNTP_ESP8266WiFi/TimeNTP_ESP8266WiFi.ino

#ifndef __TIME_PLUGIN__
#define __TIME_PLUGIN__

#define _TASK_SLEEP_ON_IDLE_RUN
#define _TASK_STD_FUNCTION

#include <Plugin.h>
#include <TimeLib.h>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <ArduinoJson.h>
#include <JsonStruct.h>
#include <utils/print.h>

using namespace std;
using namespace std::placeholders;

#define NTP_PACKET_SIZE 48 // NTP time is in the first 48 bytes of message
#define NTP_RETRY_THRESHOLD 5000 // Time in millis after last update 

// TODO add JsonStruct implementation
struct TimeConfig {
    const char* ntpServerName;
    int syncInterval;
    int timeZone;
    int offset;
};

class TimePlugin : public Plugin
{
    public:
     TimePlugin();
     TimePlugin(String ntpServerName, int timeZone, int offset, int syncInterval);
     void activate(Scheduler*);
     void syncTime();
     void updateTime();
    private:
        unsigned int udpPort = 8888;
        String ntpServerName;
        int timeZone;
        int offset;
        int syncInterval;
        int lastUpdate = 0;
        Task timeSyncTask;
        Task timeUpdateTask;
        WiFiUDP udp;
        // Buffer to hold incoming & outgoing packets
        byte packetBuffer[NTP_PACKET_SIZE];
        // Send an NTP request to the time server at the given address
        void sendNTPpacket(IPAddress &address);
        time_t getNtpTime();
        String leadingZero(int n);
        void toggleUpdate(String msg);
        
};

#endif