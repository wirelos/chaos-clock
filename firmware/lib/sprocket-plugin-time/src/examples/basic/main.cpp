#include "config.h"
#include "WiFiNet.h"
#include "Sprocket.h"
#include "TimePlugin.h"

WiFiNet *network;
Sprocket *sprocket;

void setup()
{
    sprocket = new Sprocket({STARTUP_DELAY, SERIAL_BAUD_RATE});
    sprocket->addPlugin(new TimePlugin());

    network = new WiFiNet(
        SPROCKET_MODE,
        STATION_SSID,
        STATION_PASSWORD,
        AP_SSID,
        AP_PASSWORD,
        HOSTNAME,
        CONNECT_TIMEOUT);
    network->connect();

    sprocket->activate();
}

void loop()
{
    sprocket->loop();
    yield();
} 