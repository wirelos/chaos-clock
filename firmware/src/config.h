#ifndef __STACK_DEFAULT_CONFIG__
#define __STACK_DEFAULT_CONFIG__

// FIXME distinguish FW during build
#ifndef DEFAULT_FIRMWARE
#define DEFAULT_FIRMWARE 1
#define TIMER_FIRMWARE 0
#endif

#if TIMER_FIRMWARE
#define DEFAULT_FIRMWARE 0
#define TIMER_FIRMWARE 1
#endif

// Chip config
#define SPROCKET_TYPE       "MATRIX"
#define SERIAL_BAUD_RATE    115200
#define STARTUP_DELAY       1000

// WiFi config
#define SPROCKET_MODE       1
#define WIFI_CHANNEL        11
#define MESH_PORT           5555
#define CONNECT_TIMEOUT     10000

// WiFi Access point config
#define AP_SSID             "ChaosClock"
#define AP_PASSWORD         ""
#define STATION_SSID        "myAP"
#define STATION_PASSWORD    "somepassword"
#define HOSTNAME            "chaos-clock"

// WebServer
#define WEB_CONTEXT_PATH    "/"
#define WEB_DOC_ROOT        "/www"
#define WEB_DEFAULT_FILE    "index.html"
#define WEB_PORT            80

// Matrix
#define MATRIX_HEIGHT        15
#define MATRIX_WIDTH         20
#define MATRIX_PIN           D6
#define MATRIX_BRIGHTNESS    35
#define MATRIX_TYPE          NEO_MATRIX_TOP  + NEO_MATRIX_RIGHT + NEO_MATRIX_ROWS + NEO_MATRIX_ZIGZAG
#define MATRIX_LED_TYPE      NEO_GRB + NEO_KHZ800
#define MATRIX_WS_CMD        "/cmd"
#define MATRIX_WS_STREAM     "/stream"
#define MATRIX_CONFIG_FILE   "/config/matrix.json"

// MP3 Player
#define MP3PLAYER_PIN_RX     D3
#define MP3PLAYER_PIN_TX     D4

// NTP
#define NTP_SERVER           "ch.pool.ntp.org"    // NTP server pool
#define NTP_SYNC_INTERVAL    300000               // Inverval for syncing time in milliseconds
#define NTP_TIMEZONE         1                    // Timezone used for time calculation. 1 = Central European Time
#define NTP_OFFSET           1                    // Offset to timezone

// MQTT
#if TIMER_FIRMWARE
#define MQTT_CLIENT_ID       "timer"
#define MQTT_SERVER          "mqtt-server"
#define MQTT_PORT            1883
#define MQTT_ROOT_TOPIC      "wirelos/matrix"
#define MQTT_USER            ""
#define MQTT_PASSWORD        ""
#endif

// Scheduler config
#define _TASK_SLEEP_ON_IDLE_RUN
#define _TASK_STD_FUNCTION
#define _TASK_PRIORITY

#endif
