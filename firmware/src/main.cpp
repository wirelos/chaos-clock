#include "config.h"

#if DEFAULT_FIRMWARE

#include "sprocket/system.hpp"
#include "plugins.hpp"

#define ACTOR [](const char* topic, const char* msg) -> TXO

actor_t toggle_time = ACTOR {
    int tgl = strcmp(msg, "true") == 0;
    return { topic, tgl ? "0" : "1" };
};

void user_init(SprocketStack *stk) {
    init_plugins(stk);
    route(stk, "ntp/timeShort",    "matrix/line1");
    route(stk, "ntp/full",         "matrix/scrollText");
    route(stk, "matrix/remote",    "ntp/toggleUpdate", toggle_time);
}

// base system routines
void setup() { system_init(); }
void loop()  { system_loop(); }

#endif