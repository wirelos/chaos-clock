#include "config.h"

#if TIMER_FIRMWARE

#include "sprocket/system.hpp"
#include "tasks.hpp"
#include "plugins.hpp"
#include "MatrixDisplay.h"


#define ACTOR [](const char* topic, const char* msg) -> TXO

actor_t toggleMusic = ACTOR {
    return { atoi(msg) ? "music/play" : "music/pause", "" };
}; 

actor_t timerDone = ACTOR {
    return { topic, "WRAP IT UP!!!" };
};

actor_t updateTimerDisplay = ACTOR {
    MatrixDisplayJson d;
    return { topic, d.fromConfig({
        .line1      = "Timer",
        .line1Color = LED_GREEN_HIGH,
        .line1Mode  = 0,
        .line2      = msg,
        .line2Color = LED_ORANGE_HIGH,
        .line2Mode  = 0
    })->toCharArray() };
};

actor_t updateCountdown = ACTOR {
    MatrixDisplayJson d;
    return { topic, d.fromConfig({
        .line1      = "",
        .line1Color = 0,
        .line1Mode  = 0,
        .line2      = msg,
        .line2Color = LED_RED_HIGH,
        .line2Mode  = 0
    })->toCharArray() };
};

void user_init(SprocketStack *stk) {
    // hardware init
    init_io();
    init_tasks(stk);
    init_plugins(stk);
    // event routing
    route(stk, "rotary/changed",   "matrix/display",       updateTimerDisplay);
    route(stk, "timer/countdown",  "matrix/display",       updateCountdown);
    route(stk, "timer/done",       "matrix/scrollText",    timerDone);
    route(stk, "timer/done",       "music/play");
    route(stk, "btn/pressed",      "music/pause");
    route(stk, "ntp/timeShort",    "matrix/line1");
    route(stk, "ntp/date",         "matrix/scrollText");
    // optional init
    optional_init(stk);
}

void optional_init(SprocketStack *stk) {
    stack->web->http->server
        ->serveStatic("/config/mqtt.json", SPIFFS, "config/mqtt.json");
}

// base system routines
void setup() { system_init(); }
void loop()  { system_loop(); }
#endif