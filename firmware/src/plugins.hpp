#ifndef _PLUGINS_
#define _PLUGINS_

#include "config.h"
#include "Sprocket.h"

#include "MatrixPlugin.h"
#include "TimePlugin.h"

#include "images/beer.h"
#include "images/smiley.h"

#if TIMER_FIRMWARE
#include "MqttPlugin.h"
#include "MP3PlayerPlugin.h"
#endif

void init_plugins(Sprocket* sprocket);
void init_optional_plugins(Sprocket* sprocket);

MatrixPlugin* matrixPlugin;
TimePlugin* timePlugin;

void add_images()
{
    matrixPlugin->addStaticImage({ "beer", 6, 3, 10, 10, (const uint16_t*)Beer });
    matrixPlugin->addStaticImage({ "smiley", 6, 0, 8, 8, (const uint16_t*)Smiley });
}

void init_plugins(Sprocket* sprocket) {
    timePlugin = new TimePlugin(NTP_SERVER, NTP_TIMEZONE, NTP_OFFSET, NTP_SYNC_INTERVAL);
    matrixPlugin = new MatrixPlugin({MATRIX_HEIGHT, MATRIX_WIDTH, MATRIX_PIN, MATRIX_TYPE, MATRIX_LED_TYPE, MATRIX_BRIGHTNESS });
    add_images();

    sprocket->addPlugin(timePlugin);
    sprocket->addPlugin(matrixPlugin);

    init_optional_plugins(sprocket);
}

void init_optional_plugins(Sprocket* sprocket) {
    #if TIMER_FIRMWARE
    sprocket->addPlugin(new MP3PlayerPlugin(MP3PLAYER_PIN_RX, MP3PLAYER_PIN_TX));
    sprocket->addPlugin(new MqttPlugin({MQTT_CLIENT_ID, MQTT_SERVER, MQTT_PORT, MQTT_ROOT_TOPIC, MQTT_USER, MQTT_PASSWORD}));
    #endif
}

#endif