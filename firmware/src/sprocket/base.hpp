#ifndef _BASE_STACK_
#define _BASE_STACK_

#include "config.h"

#include "Sprocket.h"
#include "WiFiNet.h"

#include "router.hpp"
#include "web.hpp"

WiFiNet* wifi() {
    return new WiFiNet(
        SPROCKET_MODE,
        STATION_SSID,
        STATION_PASSWORD,
        AP_SSID,
        AP_PASSWORD,
        HOSTNAME,
        CONNECT_TIMEOUT
    );
}

class SprocketStack : public Sprocket {
    public:
        Web *web;
        SprocketStack(SprocketConfig cfg, WebServerConfig webCfg) : Sprocket(cfg) {
            web = new Web(webCfg);
            web->addTo(this);
        }
};

#endif