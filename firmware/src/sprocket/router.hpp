#ifndef _ROUTER_
#define _ROUTER_

#include "Sprocket.h"

struct TXO {
    const char* topic;
    const char* msg;
};

typedef std::function<TXO(const char* topic,  const char* msg)> actor_t;

actor_t pass = [](const char* t,  const char* m) -> TXO {
    return { t, m };
};

auto route = [](Sprocket *s, const char* fromTopic,  const char* toTopic, actor_t relay = pass){
    s->subscribe(fromTopic, bind([s, relay, toTopic](String msg){
        TXO txo = relay(toTopic, msg.c_str());
        if(txo.topic == NULL) return;
        s->publish(txo.topic, txo.msg);
    }, _1));
};


#endif