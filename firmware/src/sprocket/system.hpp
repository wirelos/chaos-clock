#ifndef _SPROCKET_SYSTEM_
#define _SPROCKET_SYSTEM_

#include "base.hpp"
#include "router.hpp"

// TODO generalize stack
SprocketStack *stk;
Network  *network;

// user and optional stack initializers to be implemented in main.cpp
void user_init(SprocketStack *stk);
void optional_init(SprocketStack *stk);

void system_init()
{
    stk = new SprocketStack( 
        { STARTUP_DELAY, SERIAL_BAUD_RATE },
        { WEB_CONTEXT_PATH, WEB_DOC_ROOT, WEB_DEFAULT_FILE, WEB_PORT } 
    );

    user_init(stk);

    network = wifi();
    network->connect();

    stk->activate();
}

void system_loop()
{
    stk->loop();
    yield();
}

#endif