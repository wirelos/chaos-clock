#ifndef __SPROCKET_UTILS__
#define __SPROCKET_UTILS__

// Limit a value to given min/max
int bounds(int min, int max, int val ) {
    int value = val < max ? val : max;
    value = value > min ? value : min;
    return value;
}

// Limit a value to given min/max by cycling
int rebounds(int min, int max, int val ) {
    int value = val <= max ? val : min;
    value = value >= min ? value : max;
    return value;
}

#endif