#ifndef __WEB_STACK__
#define __WEB_STACK__

#include "WebServerConfig.h"
#include "WebServerPlugin.h"
#include "WebConfigPlugin.h"
#include "WebApiPlugin.h"

class Web {
    
    public:    
        WebServerPlugin *http;
        WebConfigPlugin *config;
        WebApiPlugin *api;

        Web(WebServerConfig cfg){
            http   = new WebServerPlugin( cfg );
            config = new WebConfigPlugin( http->server );
            api    = new WebApiPlugin( http->server );
        }
        void addTo(Sprocket* s) {
            s->addPlugin( config );
            s->addPlugin( api );
            s->addPlugin( http );
        }
};

#endif