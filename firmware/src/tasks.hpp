#ifndef _SPROCKET_TASKS_
#define _SPROCKET_TASKS_

#include "Sprocket.h"
#include "ec11.hpp"

using namespace a21;

// FIXME do not keep global variables

// Tasks
Task btnTask;
Task rotaryTask;
Task timerTask;

// Encoder with button
const int encoderPinA = D5;
const int encoderPinB = D6;
const int buttonPin = D8;
EC11 encoder;

int btnState = 0;
int cBtnState = 0;
int value = 0;
int mode = 0;

void checkEncoder() {
  encoder.checkPins(digitalRead(encoderPinA), digitalRead(encoderPinB));
}

void init_io() {
    pinMode(encoderPinA, INPUT_PULLUP);
    pinMode(encoderPinB, INPUT_PULLUP);
    pinMode(buttonPin, INPUT_PULLUP);
    attachInterrupt(digitalPinToInterrupt(encoderPinA), checkEncoder, CHANGE);
    attachInterrupt(digitalPinToInterrupt(encoderPinB), checkEncoder, CHANGE);
}

void init_tasks(Sprocket *sprocket) {

    btnTask.set(TASK_MILLISECOND * 200, TASK_FOREVER, [sprocket](){
        cBtnState = digitalRead(buttonPin);
        if(btnState != cBtnState){
            btnState = cBtnState;
            if(btnState) {
                mode = !mode;
                sprocket->publish("btn/pressed", String(mode));
            }
        }
    });
    
    rotaryTask.set(TASK_MILLISECOND * 250, TASK_FOREVER, [sprocket](){
        EC11Event e;
        //checkEncoder();
        if (encoder.read(&e)) {
            if (e.type == EC11Event::StepCW) {
                // Clock-wise.
                value += e.count;
            } else {
                // Counter clock-wise.
                value -= e.count;
            }

            value = value > 0 ? value : 0;
            sprocket->publish("rotary/changed", String(value));
        }
    });
    timerTask.set(TASK_SECOND, TASK_FOREVER, [sprocket](){
        if(mode && value > 0){
            value = value - 1;
            sprocket->publish("timer/countdown", String(value));
            if(value == 0) {
                sprocket->publish("timer/done","");
            }
        }
    });

    sprocket->addTask(btnTask);
    sprocket->addTask(rotaryTask);
    sprocket->addTask(timerTask);
}

#endif