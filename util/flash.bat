@echo off
echo "=->     __  __ __   ____   ___   _____        _____  _       ____  _____ __ __    ___  ____   <-="
echo "=->    /  ]|  |  | /    | /   \ / ___/       |     || |     /    |/ ___/|  |  |  /  _]|    \  <-="
echo "=->   /  / |  |  ||  o  ||     (   \_  _____ |   __|| |    |  o  (   \_ |  |  | /  [_ |  D  ) <-="
echo "=->  /  /  |  _  ||     ||  O  |\__  ||     ||  |_  | |___ |     |\__  ||  _  ||    _]|    /  <-="
echo "=-> /   \_ |  |  ||  _  ||     |/  \ ||_____||   _] |     ||  _  |/  \ ||  |  ||   [_ |    \  <-="
echo "=-> \     ||  |  ||  |  ||     |\    |       |  |   |     ||  |  |\    ||  |  ||     ||  .  \ <-="
echo "=->  \____||__|__||__|__| \___/  \___|       |__|   |_____||__|__| \___||__|__||_____||__|\_| <-="
echo.
echo ESP Flash Utility based on esptool
echo.
echo Flash an ESP chip with our latest Chaos firmware.
echo Caution: this program needs elevated privileges in order to access your COM port.
echo It will erase the complete flash memory and writes firmware and SPIFFS binary.
echo.
echo Available serial ports:
for /f "tokens=4" %%A in ('mode^|findstr "COM[0-9]*:"') do echo %%A
echo.
set /p port="type in COM Port of the Esp e.g. 'COM12': "
pause
python -m esptool --port %port% erase_flash
python -m esptool --port %port% --baud 256000 write_flash 0x00000 ../bin/basic/firmware.bin 0x00300000 ../bin/basic/spiffs.bin 
python -m esptool --port %port% --baud 256000 --after soft_reset verify_flash 0x00000 ../bin/basic/firmware.bin 0x00300000 ../bin/basic/spiffs.bin 
pause
